package pl.edu.pwsztar;


class User {
    private String firstName;
    private String lastName;
    private int age;
    private String password;

    public User(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.password = null;
    }

    public void savePassword(final String password) {
        this.password = password;
    }

    public void deletePassword() {
        this.password = null;
    }

    public boolean checkPassword(final String password) {
        return this.password != null && this.password.equals(password);
    }
}