package pl.edu.pwsztar;

class Main {

    public static void main(String[] args) {
        User bruceLee = new User("Bruce", "Lee", 24);
        bruceLee.deletePassword();
        bruceLee.savePassword("bruce24");

        System.out.printf("password %s is %s \n", "1234", bruceLee.checkPassword("1234"));
        System.out.printf("password %s is %s \n", "bruce24", bruceLee.checkPassword("bruce24"));
        bruceLee.deletePassword();
        System.out.printf("password %s is %s \n", "1234", bruceLee.checkPassword("1234"));
        System.out.printf("password %s is %s \n", "bruce24", bruceLee.checkPassword("bruce24"));

        Distance distance = new Distance();

        System.out.println(distance.methodCount(new Earth(1,2,3), new Earth(3,4,5)));

    }
}